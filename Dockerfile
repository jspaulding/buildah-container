FROM debian:stable-slim
RUN apt-get update && apt install -qq --no-install-recommends --yes \
  ca-certificates \
  wget \
  gnupg2
RUN echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
RUN wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/Debian_10/Release.key -O /Release.key
RUN apt-key add - < /Release.key
RUN apt-get update && apt-get install -qq --no-install-recommends --yes buildah podman
# Make buildah/podman happy with arbitrary UID/GID in OpenShift
RUN chmod g=u /etc/passwd /etc/group
